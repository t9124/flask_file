# code = utf8

from helpers import Manager
from pprint import pprint
from app import manager


# def main():

# manager = Manager()

@manager.assign("saldo")
def ballance(manager):
    print("SALDO")
    print(f'Aktualne saldo: {manager.balance["balance"]} PLN')
    new_balance = float(input('Zmiana salda o: '))
    manager.update_balance(new_balance)
    print(f'Aktualne saldo: {manager.balance["balance"]} PLN')
    to_history = f"SALDO - Zmiana salda o {new_balance}"
    manager.add_history_log(to_history)
    return new_balance

@manager.assign("sprzedaz")
def sale_product(manager):
    print("SPRZEDAŻ")
    product = input("Produkt: ")
    if product in manager.products:
        quantity = float(input("Ilość: "))
        in_shop_quantity = manager.products[product][1]            
        if in_shop_quantity >= quantity:            
            price = float(input("Cena jednostkowa: "))            
            new_quantity = in_shop_quantity - quantity
            manager.products[product] = [price, new_quantity]
            print(f"Zaktualizowano dane dotyczące sprzedaży - {product}.")
            manager.save_to_file(manager.WAREHOUSE_FILE, manager.products)
            manager.update_balance(price * quantity)
            to_history = f"SPRZEDAŻ - Sprzedano {quantity} szt. {product} po {price} PLN"
        else:
            print("Niewystarczająca ilość produktu w magazynie.")
            to_history = f"SPRZEDAŻ - Niewystarczająca ilość produktu w magazynie."
    else:
        print("Brak produktu w magazynie.")
        to_history = f"SPRZEDAŻ - Brak produktu w magazynie."
    manager.add_history_log(to_history)
        

@manager.assign("zakup")
def buy_product(manager):
    print("ZAKUP")
    product = input("Produkt: ")
    quantity = float(input("Ilość: "))
    price = float(input("Cena jednostkowa: "))
    if product in manager.products:
        in_shop_quantity = manager.products[product][1] + quantity
        manager.products[product] = [price, in_shop_quantity]
        print(f"Zaktualizowano dane dotyczące zakupu - {product}.")
    else:
        manager.products[product] = [price, quantity]
        print(f"Dodano {product} do magazynu - ilość {quantity}")
    to_history = f"ZAKUP - Kupiono {quantity} szt. {product} po {price} PLN"
    manager.add_history_log(to_history)
    manager.save_to_file(manager.WAREHOUSE_FILE, manager.products)
    manager.update_balance(-price * quantity)

@manager.assign("konto")
def account(manager):
    print("KONTO")
    print(f'Aktualny stan konta: {manager.balance["balance"]} PLN')
    to_history = f'KONTO - Aktualny stan konta: {manager.balance["balance"]} PLN'
    manager.add_history_log(to_history)

@manager.assign("magazyn")
def warehouse(manager):
    print("MAGAZYN")
    pprint(manager.products)
    to_history = f"MAGAZYN - {manager.products}"
    manager.add_history_log(to_history)

@manager.assign("przeglad")
def show(manager):
    print("PRZEGLĄD")
    pprint(manager.history)
    to_history = f"PRZEGLĄD"
    manager.add_history_log(to_history)

# while True:
#     print(f"Dozwolone komendy: {manager.ALLOWED_COMMANDS}")
#     command = input("Podaj komendę: ")
#     if command not in manager.ALLOWED_COMMANDS:
#         print("Niepoprawna komenda!")
#         continue
#     else:
#         match command:
#             case "stop":
#                 print("KONIEC PROGRAMU!")
#                 break
#             case "saldo":
#                 manager.execute("saldo")
#             case "sprzedaz":
#                 manager.execute("sprzedaz")
#             case "zakup":
#                 manager.execute("zakup")
#             case "konto":
#                 manager.execute("konto")
#             case "magazyn":
#                 print("MAGAZYN")
#                 manager.execute("magazyn")
#             case "przeglad":
#                 manager.execute("przeglad")

# if __name__ == "__main__":
#     main()