# code = utf8

import json
from datetime import datetime
from pprint import pprint


class Manager:
    def __init__(self):

        self.ALLOWED_COMMANDS = ("saldo", "sprzedaz", "zakup",
                        "konto", "magazyn", "przeglad",
                        "stop")
        
        self.BALANCE_FILE = "balance.txt"
        self.WAREHOUSE_FILE = "warehouse.txt"
        self.HISTORY_FILE = "history_log.txt"

        self.balance = self.load_to_cache(self.BALANCE_FILE)
        self.products = self.load_to_cache(self.WAREHOUSE_FILE)
        self.history = self.load_to_cache(self.HISTORY_FILE)

        self.actions = {}

    def load_to_cache(self, file_to_read):
        try:
            with open(file_to_read, "r", encoding='utf-8') as file:
                data = json.load(file)
        except:
            print(f"Niepoprawny format pliku {file_to_read}!")
            return None
        return data

    def save_to_file(self, file_to_save, data_to_save):
        try:
            with open(file_to_save, "w", encoding='utf-8') as file:
                data = json.dump(data_to_save, file, indent=4)
        except:
            print(f"Niepoprawny zapis do pliku {file_to_save}!")
            return None
        return True

    def update_balance(self, new_balance):
        self.balance["balance"] = new_balance
        with open(self.BALANCE_FILE, 'w') as file:
            json.dump(self.balance, file)

    def add_history_log(self, to_history):
        date = str(datetime.now())
        self.history[date] = to_history
        with open(self.HISTORY_FILE, 'w', encoding='utf-8') as file:
            json.dump(self.history, file, indent=2)
    
    def assign(self, name):
        def decorate(cb):
            self.actions[name] = cb
        return decorate

    def execute(self, name):
        if name not in self.actions:
            print("Action not defined")
        else:
            self.actions[name](self)