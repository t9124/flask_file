from flask import Flask, render_template, url_for, request
from helpers import Manager
import itertools
from pprint import pprint

app = Flask(__name__)

manager = Manager()

@app.route("/", methods=['GET', 'POST'])
def index():
    balance = manager.balance["balance"]
    balance = float(balance)
    if request.form.get("saldo") and request.form.get("komentarz"):
        manager.execute("saldo")
        balance = manager.balance["balance"]
    if request.form.get("zakup_produkt") and request.form.get("zakup_cena") and request.form.get("zakup_ilosc"):
        manager.execute("zakup")
        balance = manager.balance["balance"]
    if request.form.get("sprzedaz_produkt") and request.form.get("sprzedaz_cena") and request.form.get("sprzedaz_ilosc"):
        manager.execute("sprzedaz")
        balance = manager.balance["balance"]
    products = manager.products

    return render_template('index.html', balance=balance,
                                        products=products)

@app.route("/historia/")
@app.route("/historia/<int:line_from>/<int:line_to>")
def historia(line_from=0, line_to=len(manager.history)):
    line_from = int(line_from)
    line_to = int(line_to) + 1
    history = manager.history
    history = dict(sorted(itertools.islice(history.items(), line_from, line_to), reverse=True))
    return render_template('historia.html', history=history)

@manager.assign("saldo")
def ballance(manager):
    print("SALDO")
    new_balance = request.form.get("saldo")
    new_balance = float(new_balance)
    komentarz = request.form.get("komentarz")
    to_history = f"SALDO - Zmiana salda o {new_balance}. Komentarz: {komentarz}"
    balance = float(manager.balance["balance"])
    new_balance = new_balance + balance
    manager.update_balance(new_balance)    
    manager.add_history_log(to_history)
    print(new_balance)
    return new_balance

@manager.assign("sprzedaz")
def sale_product(manager):
    print("SPRZEDAŻ")
    product = request.form.get("sprzedaz_produkt")
    print(product)
    if product in manager.products:
        quantity = float(request.form.get("sprzedaz_ilosc"))
        print(quantity)
        in_shop_quantity = manager.products[product][1]            
        if in_shop_quantity >= quantity:            
            price = float(request.form.get("sprzedaz_cena"))
            print(price)
            new_quantity = in_shop_quantity - quantity
            manager.products[product] = [price, new_quantity]
            manager.save_to_file(manager.WAREHOUSE_FILE, manager.products)
            new_balance = float(manager.balance["balance"]) + (price * quantity)
            manager.update_balance(new_balance)
            to_history = f"SPRZEDAŻ - Sprzedano {quantity} szt. {product} po {price} PLN"
        else:
            to_history = f"SPRZEDAŻ - Niewystarczająca ilość produktu w magazynie."
    else:
        to_history = f"SPRZEDAŻ - Brak produktu w magazynie."
    manager.add_history_log(to_history)

@manager.assign("zakup")
def buy_product(manager):
    print("ZAKUP")
    product = request.form.get("zakup_produkt")
    quantity = float(request.form.get("zakup_ilosc"))
    price = float(request.form.get("zakup_cena"))
    if product in manager.products:
        in_shop_quantity = manager.products[product][1] + quantity
        manager.products[product] = [price, in_shop_quantity]
        print(f"Zaktualizowano dane dotyczące zakupu - {product}.")
    else:
        manager.products[product] = [price, quantity]
        print(f"Dodano {product} do magazynu - ilość {quantity}")
    to_history = f"ZAKUP - Kupiono {quantity} szt. {product} po {price} PLN"
    manager.add_history_log(to_history)
    manager.save_to_file(manager.WAREHOUSE_FILE, manager.products)
    new_balance = float(manager.balance["balance"]) - (price * quantity)
    manager.update_balance(new_balance)

@manager.assign("konto")
def account(manager):
    print("KONTO")
    print(f'Aktualny stan konta: {manager.balance["balance"]} PLN')
    to_history = f'KONTO - Aktualny stan konta: {manager.balance["balance"]} PLN'
    manager.add_history_log(to_history)

@manager.assign("magazyn")
def warehouse(manager):
    print("MAGAZYN")
    pprint(manager.products)
    to_history = f"MAGAZYN - {manager.products}"
    manager.add_history_log(to_history)

@manager.assign("przeglad")
def show(manager):
    print("PRZEGLĄD")
    pprint(manager.history)
    to_history = f"PRZEGLĄD"
    manager.add_history_log(to_history)